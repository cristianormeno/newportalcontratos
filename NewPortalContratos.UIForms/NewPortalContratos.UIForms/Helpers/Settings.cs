﻿namespace NewPortalContratos.UIForms.Helpers
{
    using Newtonsoft.Json;
    using Models;
    using Plugin.Settings;
    using Plugin.Settings.Abstractions;

    public static class Settings
    {
        static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        // Valores x Defecto
        static readonly string stringDefault = string.Empty;
        static readonly int intDefault = 0;
        static readonly bool boolDefault = false;

        //Recordarme
        const string recordarme = "Recordarme";
        public static bool Recordarme
        {
            get
            {
                return AppSettings.GetValueOrDefault(recordarme, boolDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(recordarme, value);
            }
        }

        //Usuario
        const string usuario = "Usuario";
        public static UsuarioModel Usuario
        {
            get
            {

                string Valor = AppSettings.GetValueOrDefault(usuario, string.Empty);
                UsuarioModel miObj;
                if (string.IsNullOrEmpty(Valor))
                    miObj = new UsuarioModel();
                else
                    miObj = JsonConvert.DeserializeObject<UsuarioModel>(Valor);
                return miObj;

            }
            set
            {
                string Valor = JsonConvert.SerializeObject(value);
                AppSettings.AddOrUpdateValue(usuario, Valor);
            }

        }

        //Notificaciones
        const string notificaciones = "Notificaciones";
        public static string Notificaciones
        {
            get { return AppSettings.GetValueOrDefault(notificaciones, stringDefault); }
            set { AppSettings.AddOrUpdateValue(notificaciones, value); }
        }

        //Notificaciones Pendientes
        const string numeroNotificaciones = "NumeroNotificaciones";
        public static int NumeroNotificaciones
        {
            get { return AppSettings.GetValueOrDefault(numeroNotificaciones, intDefault); }
            set { AppSettings.AddOrUpdateValue(numeroNotificaciones, value); }
        }

        // Token de Dispositivo
        const string tokenDispositivo = "Token";
        public static string TokenDispositivo
        {
            get { return AppSettings.GetValueOrDefault(tokenDispositivo, stringDefault); }
            set { AppSettings.AddOrUpdateValue(tokenDispositivo, value); }
        }

        //Notificación seleccionada
        const string notificacionSeleccionada = "NotificacionSeleccionada";
        public static NotificacionModel NotificacionSeleccionada
        {
            get
            {

                string Valor = AppSettings.GetValueOrDefault(notificacionSeleccionada, string.Empty);
                NotificacionModel miObj;
                if (string.IsNullOrEmpty(Valor))
                    miObj = new NotificacionModel();
                else
                    miObj = JsonConvert.DeserializeObject<NotificacionModel>(Valor);
                return miObj;

            }
            set
            {
                string Valor = JsonConvert.SerializeObject(value);
                AppSettings.AddOrUpdateValue(notificacionSeleccionada, Valor);
            }

        }

    }
}
