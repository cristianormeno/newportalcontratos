﻿namespace NewPortalContratos.UIForms.Services
{
    public static class VersionUnica
    {
        #region Atributos
        // Versión Unica de la aplicación
        private static int versionApp = 1;
        #endregion

        #region Metodos
        public static bool ValidarVersion(int VersionMinima)
        {
            // Si la versión única de la APP 
            // es menor a la versión mínima del servidor, devuelve falso
            if (versionApp < VersionMinima)
                return false;
            else
                return true;
        }
        #endregion



    }
}
