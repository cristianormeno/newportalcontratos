﻿namespace NewPortalContratos.UIForms.Models
{
    public class UsuarioModel
    {
        public int UsuarioID { get; set; } = 0;
        public string Usuario { get; set; } = string.Empty;
        public string Contraseña { get; set; } = string.Empty;
        public string Mandante { get; set; } = string.Empty;
        public string TokenApiWeb { get; set; } = string.Empty;
        public string Nombre { get; set; } = string.Empty;
        public string URL { get; set; } = string.Empty;
    }
}

