﻿using System;

namespace NewPortalContratos.UIForms.Models
{
    public class NotificacionModel
    {
        //--- Datos
        public string Id { get; set; }
        public DateTime Fecha { get; set; }
        public string Titulo { get; set; }
        public string Mensaje { get; set; }
        public string Semaforo { get; set; }
        public string Contratista { get; set; }

        //--- Formato
        public bool ConSemaforo { get; set; }
        public bool MostrarSemaforoListado { get; set; }
        public bool MostrarSemaforoDetalle { get; set; }

        //--- sólo control
        public bool Leido { get; set; }

    }
}
