﻿using Newtonsoft.Json;

namespace NewPortalContratos.UIForms.Models
{
    public class MenuModel
    {
        [JsonProperty("Grupo")]
        public string Grupo { get; set; } = string.Empty;

        [JsonProperty("Icono")]
        public string Icono { get; set; } = string.Empty;

        [JsonProperty("Titulo")]
        public string Titulo { get; set; } = string.Empty;

        [JsonProperty("Destino")]
        public string Destino { get; set; } = string.Empty;

        [JsonProperty("Tipo")]
        public int Tipo { get; set; } = 1;
    }
}
