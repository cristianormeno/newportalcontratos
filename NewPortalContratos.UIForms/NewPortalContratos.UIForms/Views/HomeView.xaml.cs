﻿namespace NewPortalContratos.UIForms.Views
{
    using ViewModels;
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeView : ContentPage
    {
        public HomeView()
        {
            InitializeComponent();
            this.BindingContext = new HomeViewModel(Navigation, WebView1);
        }

        void webviewNavigating(object sender, WebNavigatingEventArgs e)
        {
            labelLoading.Text = Helpers.Languages.Cargando + " ... ";
            labelLoading.IsVisible = true;
        }

        void webviewNavigated(object sender, WebNavigatedEventArgs e)
        {
            labelLoading.Text = Helpers.Languages.Cargando + " ... ";
            labelLoading.IsVisible = false;
        }
    }
}