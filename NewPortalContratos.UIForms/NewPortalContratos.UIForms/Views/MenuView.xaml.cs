﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NewPortalContratos.UIForms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuView : ContentPage
    {
        public MenuView()
        {
            InitializeComponent();
        }
    }
}