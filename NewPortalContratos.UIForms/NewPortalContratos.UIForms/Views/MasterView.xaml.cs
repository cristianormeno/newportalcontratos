﻿namespace NewPortalContratos.UIForms.Views
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterView : MasterDetailPage
    {
        public MasterView()
        {
            InitializeComponent();
            App.Navigator = Navigator;
            App.Master = this;
        }

    }       
}