﻿namespace NewPortalContratos.UIForms.Views
{
    using NewPortalContratos.UIForms.ViewModels;
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotificacionesView : ContentPage
    {
        public NotificacionesView()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MainViewModel.GetInstance().Notificaciones = new NotificacionesViewModel();


        }
    }
}