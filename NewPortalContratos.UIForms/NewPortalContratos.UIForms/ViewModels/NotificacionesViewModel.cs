﻿namespace NewPortalContratos.UIForms.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Helpers;
    using Models;
    using Notifications;
    using NewPortalContratos.UIForms.Views;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Xamarin.Forms;
    using NewPortalContratos.UIForms.Common;

    public class NotificacionesViewModel : BaseViewModel
    {
        #region Attributes
        private ObservableCollection<NotificacionModel> notificacionesCollection;
        private ObservableCollection<NotificacionModel> ListaOrdenada;
        private ObservableCollection<NotificacionItemViewModel> notificaciones;
        private bool estaRecargando;
        #endregion

        #region Properties
        public ObservableCollection<NotificacionItemViewModel> Notificaciones
        {
            get { return this.notificaciones; }
            set { SetValue(ref this.notificaciones, value); }
        }

        public bool EstaRecargando
        {
            get { return this.estaRecargando; }
            set { SetValue(ref this.estaRecargando, value); }
        }
        #endregion

        #region Constructors
        public NotificacionesViewModel()
        {
            this.CargarNotificaciones();
            EliminarCommand = new Command<NotificacionItemViewModel>(async (model) => await EliminarNotificacion(model));
        }

        #endregion

        #region Comandos

        public ICommand RecargarCommand
        {
            get {return new RelayCommand(CargarNotificaciones);}
        }

        public ICommand EliminarCommand { get; private set; }
               

        #endregion

        #region Metodos


        private void CargarNotificaciones()
        {
            this.EstaRecargando = true;

            this.notificacionesCollection = NotificationsServices.GetNotificaciones();

            ListaOrdenada = new ObservableCollection<NotificacionModel>(this.notificacionesCollection.OrderByDescending(i => i.Fecha));
            this.Notificaciones = new ObservableCollection<NotificacionItemViewModel>(this.ToNotificacionItemViewModel());
            this.EstaRecargando = false;

        }

        private IEnumerable<NotificacionItemViewModel> ToNotificacionItemViewModel()
        {
            return ListaOrdenada.Select(l => new NotificacionItemViewModel
            {
                Id = l.Id,
                Fecha = l.Fecha,

                Titulo = l.Titulo,
                Mensaje = l.Mensaje,
                Semaforo = l.Semaforo,
                Contratista = l.Contratista,

                ConSemaforo = l.ConSemaforo,
                MostrarSemaforoListado = l.MostrarSemaforoListado,
                MostrarSemaforoDetalle = l.MostrarSemaforoDetalle,

                TextoContratista = "Contratista:",

                Leido = l.Leido,
                ColorFondo01 = (l.Leido) ? "#D9D9D9" : "#FFFFFF",
                ColorFondo02 = (l.Leido) ? "#999999" : "#E55500",

                ColorTexto01 = (l.Leido) ? "#585858" : "#585858",
                ColorTexto02 = (l.Leido) ? "#585858" : "#585858",
                ColorTexto03 = (l.Leido) ? "#585858" : "#585858",

                ColorBorde01 = (l.Leido) ? "#252525" : "#252525",
                ColorBorde02 = (l.Leido) ? "#252525" : "#252525"
            });
        }

        private async Task EliminarNotificacion(NotificacionItemViewModel notificacion)
        {
            Acr.UserDialogs.UserDialogs.Instance.Confirm(new Acr.UserDialogs.ConfirmConfig
            {
                CancelText = Languages.No,
                OkText = Languages.Si,
                Message = Languages.EliminarNotificacionMensaje,
                OnAction = async (r) =>
                {
                    if (r)
                    {
                        NotificationsServices.DeleteNotificacion(notificacion.Id);
                        NotificationsServices.NotificacionesPendientes();
                        CargarNotificaciones();
                    }
                }
            });

        }



        #endregion
    }
}
