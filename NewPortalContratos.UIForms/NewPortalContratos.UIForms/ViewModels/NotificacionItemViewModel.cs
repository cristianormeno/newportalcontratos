﻿namespace NewPortalContratos.UIForms.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Helpers;
    using Notifications;
    using System.Windows.Input;
    using Views;

    public class NotificacionItemViewModel : NotificacionModel
    {
        #region Propiedades
        public string ColorFondo01 { get; set; }
        public string ColorFondo02 { get; set; }

        public string ColorTexto01 { get; set; }
        public string ColorTexto02 { get; set; }
        public string ColorTexto03 { get; set; }

        public string ColorBorde01 { get; set; }
        public string ColorBorde02 { get; set; }

        public string TextoContratista { get; set; }
        #endregion

        #region Commands
        public ICommand SeleccionarNotificacionCommand
        {
            get
            {
                return new RelayCommand(SeleccionarNotificacion);
            }
        }

        #endregion

        #region Metodos

        private async void SeleccionarNotificacion()
        {
            MainViewModel.GetInstance().Notificacion = new NotificacionViewModel(this);
            await App.Navigator.PushAsync(new NotificacionView());
        }


        #endregion
    }
}
