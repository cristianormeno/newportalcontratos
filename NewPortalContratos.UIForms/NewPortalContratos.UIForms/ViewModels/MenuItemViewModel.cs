﻿namespace NewPortalContratos.UIForms.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Helpers;
    using Models;
    using Newtonsoft.Json;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;

    public class MenuItemViewModel
    {
        [JsonProperty("Grupo")]
        public string Grupo { get; set; } = string.Empty;

        [JsonProperty("Icono")]
        public string Icono { get; set; } = string.Empty;

        [JsonProperty("Titulo")]
        public string Titulo { get; set; } = string.Empty;

        [JsonProperty("Destino")]
        public string Destino { get; set; } = string.Empty;

        [JsonProperty("Tipo")]
        public int Tipo { get; set; } = 0;

        #region Commands
        public ICommand NavigateCommand
        {
            get
            {
                return new RelayCommand(Navigate);
            }
        }

        private void Navigate()
        {
            App.Master.IsPresented = false;

            switch (this.Tipo)
            {
                case -1:
                    switch (this.Titulo.ToLower())
                    {
                        case "home":
                            MainViewModel.GetInstance().URL = string.Empty;
                            MainViewModel.GetInstance().Home = new HomeViewModel();
                            App.Navigator.PushAsync(new HomeView());
                            break;
                        case "notificaciones":
                            MainViewModel.GetInstance().Notificaciones = new NotificacionesViewModel();
                            App.Navigator.PushAsync(new NotificacionesView());
                            break;
                        case "salir":
                            Settings.Recordarme = false;
                            UsuarioModel usu = new UsuarioModel()
                            {
                                Usuario = null,
                                Contraseña = null,
                                Mandante = null,
                                TokenApiWeb = null,
                                UsuarioID = 0,
                                URL = null
                            };


                            var mainViewModel = MainViewModel.GetInstance();
                            mainViewModel.Usuario = usu;
                            Settings.Usuario = usu;

                            Application.Current.MainPage = new NavigationPage(
                                new LoginView());
                            break;
                        
                            


                    }

                    break;
                default:
                    MainViewModel.GetInstance().URL = this.Destino;
                    MainViewModel.GetInstance().Home = new HomeViewModel();
                    App.Navigator.PushAsync(new HomeView());
                    break;
            }         










            
        }
        #endregion
    }
}
