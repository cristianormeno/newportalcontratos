﻿namespace NewPortalContratos.UIForms
{
    using Common;
    using Helpers;
    using Models;
    using Notifications;
    using Plugin.Badge;
    using Plugin.FirebasePushNotification;
    using System;
    using System.Threading.Tasks;
    using ViewModels;
    using Views;
    using Xamarin.Forms;


    public partial class App : Application
    {
        #region Properties
        public static NavigationPage Navigator {  get; internal set;}
        public static MasterView Master { get; internal set; }
        #endregion

        #region Constructors
        public App()
        {
            InitializeComponent();
            try
            {
                Settings.NumeroNotificaciones = NotificationsServices.NoLeidas();
                if (Settings.NumeroNotificaciones == 0)
                {
                    CrossBadge.Current.ClearBadge();
                }
                
                //Obtiene el Token del dispositivo para Notificaciones
                CrossFirebasePushNotification.Current.OnTokenRefresh += (s, p) =>
                {

                    //Recupero el Token del dispositivo para las notificaciones
                    Settings.TokenDispositivo = p.Token;
                    System.Diagnostics.Debug.WriteLine($"TOKEN: {p.Token}");
                };

                //Cuando ingresa una notificación 
                CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
                {
                    //Notificación recibida
                    System.Diagnostics.Debug.WriteLine("Received");
                    if (p.Data != null)
                    {
                        NotificationsServices.CreateNotificacion(p.Data);
                    }

                };

                //Cuando se abre la aplicación atravez de una notificación
                CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
                {
                    NotificacionModel NotificacionCreada = NotificationsServices.NotificacionCreadaAcceso(p.Data);
                    Settings.NotificacionSeleccionada = NotificacionCreada;
                    if (Settings.NotificacionSeleccionada != null && !string.IsNullOrEmpty(Settings.TokenDispositivo) && !string.IsNullOrEmpty(Settings.Usuario.TokenApiWeb))
                    {
                        MainViewModel.GetInstance().Notificacion = new NotificacionViewModel(Settings.NotificacionSeleccionada);

                        if (Device.RuntimePlatform.ToString().ToUpper() != "ANDROID")
                        {
                            Settings.NotificacionSeleccionada = null;
                        }
                        App.Navigator.PushAsync(new NotificacionView());
                    }
                };

                //Consultar por la opción de recordar usuario
                if (Settings.Recordarme == true)
                {
                    // Opcion recordar habilitada
                    //Creo la instancia de la MainViewModel
                    MainViewModel mainViewModel = MainViewModel.GetInstance();

                    // Obtengo el Token Web guardado
                    string TokenApiWeb = Settings.Usuario.TokenApiWeb;

                    if (string.IsNullOrEmpty(TokenApiWeb))
                    {
                        //No existe TokenApiWeb, debo obligar a autenticarse al usuario
                        this.MainPage = new NavigationPage(new LoginView());
                    }
                    else
                    {
                        Common.Common comunes = new Common.Common();

                        // Validar que el token esté vigente
                        bool TokenVigente = comunes.TokenVigente(TokenApiWeb);

                        if (TokenVigente) 
                        {
                            // El token está vigente

                            //Intento hacer login con los datos guardados
                            Autenticar aut = new Autenticar();

                            aut.Login();
                            Application.Current.MainPage = new MasterView();

                            //if (string.IsNullOrEmpty(Settings.Usuario.TokenApiWeb))
                            //{
                            //    // No pudo autenticarse automáticamente, redirijo a Login
                            //    this.MainPage = new NavigationPage(new LoginView());
                            //}
                            //else
                            //{
                            //    // Valido el nuevo token obtenido
                            //    TokenApiWeb = Settings.Usuario.TokenApiWeb;

                            //    if (comunes.TokenVigente(TokenApiWeb))
                            //    {
                            //        // El token está vigente, redirijo al Home
                            //        mainViewModel.Usuario = Settings.Usuario;
                            //        mainViewModel.Home = new HomeViewModel();
                            //        Application.Current.MainPage = new MasterView();
                            //    }
                            //    else
                            //    {
                            //        this.MainPage = new NavigationPage(new LoginView());
                            //    }

                            //}

                        }
                        else
                        {
                            //Token vencido, debo obligar a autenticarse al usuario
                            this.MainPage = new NavigationPage(new LoginView());
                        }

                    }
                }
                else
                {
                    //Opcion recordar usuario deshabilitada, redirijo a Login
                    this.MainPage = new NavigationPage(new LoginView());
                }
                
                // Habrir notificación si el dispositivo es ANDROID
                //if (Device.RuntimePlatform.ToString().ToUpper() == "ANDROID")
                //{
                //    if (Settings.NotificacionSeleccionada != null && !string.IsNullOrEmpty(Settings.TokenDispositivo) && !string.IsNullOrEmpty(Settings.Usuario.TokenApiWeb))
                //    {
                //        MainViewModel.GetInstance().Notificacion = new NotificacionViewModel(Settings.NotificacionSeleccionada);
                //        //MainViewModel.GetInstance().Notificaciones = new NotificacionesViewModel();
                //        Settings.NotificacionSeleccionada = null;
                //        App.Navigator.PushAsync(new NotificacionView());
                //        //  App.Navigator.PushAsync(new NotificacionesView());
                //    }
                //}
            }
            catch (Exception ex)
            { }



        }

        #endregion

     

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
