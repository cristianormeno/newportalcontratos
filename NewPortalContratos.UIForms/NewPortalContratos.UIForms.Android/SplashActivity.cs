﻿namespace NewPortalContratos.UIForms.Droid
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;
    using Plugin.FirebasePushNotification;

    //[Activity( Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true)]
    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);


            System.Threading.Thread.Sleep(2000);

            this.StartActivity(typeof(MainActivity));

            FirebasePushNotificationManager.ProcessIntent(this,Intent);
        }

        protected override void OnNewIntent(Intent intent)
        {
            FirebasePushNotificationManager.ProcessIntent(this,intent);
            base.OnNewIntent(intent);
        }
    }
}