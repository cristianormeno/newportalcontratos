﻿namespace NewPortalContratos.UIForms.Droid
{
    using Android.App;
    using Android.OS;
    using Android.Runtime;
    using Plugin.FirebasePushNotification;
    using System;
    using NewPortalContratos.UIForms.Helpers;
    using NewPortalContratos.UIForms.Notifications;
    using Firebase;
    using Android.Support.V4.Content;
    using Acr.UserDialogs;
    using NewPortalContratos.UIForms.Models;

    [Application]
    public class MainApplication : Application//, Application.IActivityLifecycleCallbacks
    {
        static readonly string TAG = "MainApplication";

        internal static readonly string CHANNEL_ID = "my_notification_channel";
        internal static readonly int NOTIFICATION_ID = 100;

        public MainApplication(IntPtr handle, JniHandleOwnership transer) : base(handle, transer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            FirebaseApp app = FirebaseApp.InitializeApp(Android.App.Application.Context);

            //Set the default notification channel for your app when running Android Oreo
            CreateNotificationChannel();

            //If debug you should reset the token each time.

#if DEBUG
            FirebasePushNotificationManager.Initialize(this, true);
#else
            FirebasePushNotificationManager.Initialize(this, false);
#endif


            //Handle notification when app is closed here
            CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine("NOTIFICATION RECEIVED", p.Data);


            };

            //Cuando se abre la aplicación atravez de una notificación
            CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine("NOTIFICATION OPENED", p.Data);

                NotificacionModel NotificacionCreada = NotificationsServices.NotificacionCreadaAcceso(p.Data);

                Settings.NotificacionSeleccionada = NotificacionCreada;

            };


        }

        void CreateNotificationChannel()
        {
            try
            {
                //UserDialogs.Instance.Alert("Android-CreateNotificationChannel");
                if (Build.VERSION.SdkInt < BuildVersionCodes.O)
                {
                    // Notification channels are new in API 26 (and not a part of the
                    // support library). There is no need to create a notification
                    // channel on older versions of Android.
                    return;
                }


                //// Codigo agregado


                /// fin código agregado

                var channel = new NotificationChannel(CHANNEL_ID, "PortalContratos", NotificationImportance.High)
                {
                    Description = "Canal FCM para APP PortalContratos"
                };
                channel.EnableLights(true);


                var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
                notificationManager.CreateNotificationChannel(channel);
            }
            catch (Exception ex)
            { }

        }


    }
}
