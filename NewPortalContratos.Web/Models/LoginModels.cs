﻿namespace NewPortalContratos.Web.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    public class LoginRequest
    {
        public LoginUsuario Usuario { get; set; }
        public LoginDispositivo Dispositivo { get; set; }
    }

    public class LoginResponse
    {
        public ResponseResultado Respuesta { get; set; }
        public ResponseUsuario Usuario { get; set; }
        public ResponseSistema Sistema { get; set; }
        public List<ResponseMenu> Menu { get; set; }
    }

    public partial class LoginUsuario {
        [Required]
        [Display(Description = "Usuario")]
        [StringLength(30)]
        public string Usuario { get; set; } = string.Empty;
        [Required]
        [Display(Description = "Password")]
        [StringLength(30)]
        public string Password { get; set; } = string.Empty;
        [Required]
        [Display(Description = "Mandante al que pertenece el usuario")]
        [StringLength(30)]
        public string Mandante { get; set; } = string.Empty;
    }
    public partial class LoginDispositivo
    {
        [Display(Description = "Token del Dispositivo")]
        [StringLength(300)]
        public string Token { get; set; } = string.Empty;

        [Display(Description = "Sistema operativo del dispositivo: 1_Android, 2_IOS")]
        public int Sistema { get; set; } = 0;
    }

    public partial class ResponseResultado
    {
        [JsonProperty("Resultado")]
        public string Resultado { get; set; } = string.Empty;

        [JsonProperty("Mensaje")]
        public string Mensaje { get; set; } = string.Empty;
    }

    public partial class ResponseUsuario
    {
        [JsonProperty("UsuarioId")]
        public long UsuarioId { get; set; } = 0;

        [JsonProperty("Nombre")]
        public string Nombre { get; set; } = string.Empty;

        [JsonProperty("TokenApiWeb")]
        public string TokenApiWeb { get; set; } = string.Empty;
    }

    public partial class ResponseSistema
    {
        [JsonProperty("VersionMinima")]
        public long VersionMinima { get; set; } = 0;

        [JsonProperty("VersionActual")]
        public long VersionActual { get; set; } = 0;

        [JsonProperty("VersionActualNombre")]
        public string VersionActualNombre { get; set; } = string.Empty;

        [JsonProperty("URL")]
        public string URL { get; set; } = string.Empty;
    }

    public partial class ResponseMenu
    {
        [JsonProperty("Grupo")]
        public string Grupo { get; set; } = string.Empty;

        [JsonProperty("Icono")]
        public string Icono { get; set; } = string.Empty;

        [JsonProperty("Titulo")]
        public string Titulo { get; set; } = string.Empty;

        [JsonProperty("Destino")]
        public string Destino { get; set; } = string.Empty;
    }

    
}