﻿namespace NewPortalContratos.Web.Models
{
    using System.Collections.Generic;

    public class Result
    {
        public string message_id { get; set; }
        public string error { get; set; }
    }

    public class FirebaseResponseModel
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<Result> results { get; set; }
    }
}