﻿namespace NewPortalContratos.Web.Controllers
{
    using NewPortalContratos.Web.Controllers.Login;
    using NewPortalContratos.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    [RoutePrefix("pc")]

    public class LoginController : ApiController
    {
        [HttpPost]
        [Route("Login")]
        public IHttpActionResult LogIn(LoginRequest Datos)
        {
            if (Datos == null) {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            //Variable de Respuesta
            LoginResponse Respuesta = new LoginResponse(); 

            //Obtengo la respuesta
            Respuesta = clsLogin.Login(Datos);

            return Ok(Respuesta);


            //    Respuesta=new ResponseResultado() { 
            //        Resultado="OK",
            //        Mensaje="todo bien"
            //    },
            //    Usuario=new ResponseUsuario() { 
            //        UsaurioId=0,
            //        CambioPassword=0
            //    },
            //    Sistema=new ResponseSistema() { 
            //        VersionMinima=1,
            //        VersionActual=2
            //    }

            //};

            //return Ok(Respuesta);
        }
    }
}
