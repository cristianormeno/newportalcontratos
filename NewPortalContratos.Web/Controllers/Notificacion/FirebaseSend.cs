﻿namespace NewPortalContratos.Web.Controllers.Notificacion
{
    using NewPortalContratos.Web.Models;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using NewPortalContratos.Web.Controllers.Common;

    public class FirebaseSend
    {
        public void SendNotificacion(NotificacionSendRequest noti, ref string ErrorID, ref string ErrorMensaje)
        {
            string FirebaseKey = Common.GetFirebaseKey();
            string FirebaseUri = Common.GetFirebaseUri();

            WebRequest tRequest = WebRequest.Create(FirebaseUri);
            tRequest.Method = "post";

            //if (noti.Semaforo.Trim().ToLower() == "rojo")
            //    noti.Semaforo = "red";
            //else
            //    if (noti.Semaforo.Trim().ToLower() == "amarillo")
            //    noti.Semaforo = "yellow";
            //else
            //        if (noti.Semaforo.Trim().ToLower() == "verde")
            //    noti.Semaforo = "green";
            //else
            //    noti.Semaforo = "white";


            //serverKey - Key from Firebase cloud messaging server  
            tRequest.Headers.Add(string.Format("Authorization: key={0}", FirebaseKey.Trim()));
            //Sender Id - From firebase project setting  
            //tRequest.Headers.Add(string.Format("Sender: id={0}", "XXXXX.."));
            tRequest.ContentType = "application/json";
            var payload = new
            {
                to = noti.Token.Trim(),// "e8EHtMwqsZY:APA91bFUktufXdsDLdXXXXXX..........XXXXXXXXXXXXXX",
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = noti.Mensaje,// "Test",
                    title = noti.Titulo,// "Test",
                    badge = 1
                },
                data = new
                {
                    Titulo = noti.Titulo,
                    Mensaje = noti.Mensaje,
                    Semaforo = noti.Semaforo,
                    ConSemaforo = "true",
                    MostrarSemaforoListado = "true",
                    MostrarSemaforoDetalle = "true",
                    NotificacionId = noti.NotificacionId
                }
            };

            string postbody = JsonConvert.SerializeObject(payload).ToString();
            Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                FirebaseResponseModel Resultado = JsonConvert.DeserializeObject<FirebaseResponseModel>(sResponseFromServer);
                                if (Resultado.failure > 0)
                                {
                                    ErrorID = "FCM Notification Failed";
                                    foreach (var linea in Resultado.results)
                                    {
                                        if (string.IsNullOrEmpty(ErrorMensaje))
                                        {
                                            ErrorMensaje = linea.error;
                                        }
                                    }

                                }

                                
                            }
                    }
                }
            }
        }
    }
}