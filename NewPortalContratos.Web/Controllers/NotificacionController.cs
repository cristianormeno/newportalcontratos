﻿namespace NewPortalContratos.Web.Controllers
{
    using NewPortalContratos.Web.Controllers.Notificacion;
    using NewPortalContratos.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    [RoutePrefix("pc")]

    public class NotificacionController : ApiController
    {
        [HttpPost]
        [Route("EnviarNotificacion")]
        public IHttpActionResult EnviarNotificacion(NotificacionRequest Datos)
        {
            //Variable de Respuesta
            NotificacionResponse Respuesta = new NotificacionResponse();

            try
            {

                // obtengo la respuesta
                Respuesta = clsNotificacion.EnviarNotificacion(Datos);
                return Ok(Respuesta);
            }
            catch (Exception ex)
            {
                Respuesta.Respuesta.Resultado = "ERROR";
                Respuesta.Respuesta.Mensaje = ex.Message;
                return Ok(Respuesta);
            }           
                        
            
        }

    }
}
