﻿namespace NewPortalContratos.Web.Controllers.Login
{
    using NewPortalContratos.Web.Models;
    using NewPortalContratos.Web.Controllers.Common;
    using System.Data;
    using Newtonsoft.Json;

    public static class clsLogin
    {
        public static LoginResponse Login(LoginRequest Datos) {
            // Obtengo la conexión
            string Conexion = Common.GetStringConnection(Datos.Usuario.Mandante);

            //Variable de Respuesta
            LoginResponse Respuesta = new LoginResponse();

            if (string.IsNullOrEmpty(Conexion)) {
                Respuesta.Respuesta.Resultado = "ERROR";
                Respuesta.Respuesta.Mensaje = "Mandante erróneo";
                return Respuesta;
            }   
            else {

                DataSet ds = new DataSet();

                ds = dalLogin.Login(Conexion, Datos);

                if (ds.Tables.Count > 0)
                {
                    Respuesta = JsonConvert.DeserializeObject<LoginResponse>(ds.Tables[0].Rows[0]["Respuesta"].ToString());
                    Respuesta.Sistema = new ResponseSistema()
                    {
                        VersionMinima = Common.GetVersionMinima(),
                        VersionActual = Common.GetVersionActual(),
                        VersionActualNombre = Common.GetVersionActualNombre(),
                        URL = Common.GetURL()
                    };
                    if (Respuesta.Respuesta.Resultado.ToString().ToUpper() == "OK")
                    {
                        // Genero el Token WebApi
                        var token = TokenGenerator.GenerateTokenJwt(Datos.Usuario.Usuario);
                        Respuesta.Usuario.TokenApiWeb = token;

                        // Agrego el menú Soporte
                        Respuesta.Menu.Add(new ResponseMenu()
                        {
                            Grupo = "Home",
                            Icono = "FaceAgent",
                            Titulo = "Soporte",
                            Destino = Common.GetURLSoporte()

                        });
                    }
                }
                else {
                    Respuesta = new LoginResponse() {
                        Respuesta = new ResponseResultado()
                        {
                            Resultado = "ERROR",
                            Mensaje = "Algo salió mal, consulte con el administrador del sistema"
                        },
                        Usuario = new ResponseUsuario()
                        {
                            UsuarioId = 0,
                            Nombre = string.Empty
                        },
                        Sistema = new ResponseSistema()
                        {
                            VersionMinima = Common.GetVersionMinima(),
                            VersionActual = Common.GetVersionActual(),
                            VersionActualNombre=Common.GetVersionActualNombre()
                        }

                    };
            }




                
                return Respuesta;
            }
            
        }
    
        
    }
}