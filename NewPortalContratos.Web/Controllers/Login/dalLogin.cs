﻿
using NewPortalContratos.Web.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace NewPortalContratos.Web.Controllers.Login
{
    public static class dalLogin
    {
        public static DataSet Login(string Conexion, LoginRequest Datos) {
            DataSet ds = new DataSet();
            try
            {
                //Preparo el parámetro json
                string Parametros = Newtonsoft.Json.JsonConvert.SerializeObject(Datos);

                //Preparo la ejecución del procedimiento
                SqlCommand dbCommand = new SqlCommand();

                SqlParameter par = new SqlParameter("@Parametros", SqlDbType.VarChar, -1);
                par.Direction = ParameterDirection.Input;
                par.Value = Parametros;
                dbCommand.Parameters.Add(par);

                ds = Common.Common.ConsultaSQL(Conexion, "spcLogin", dbCommand);
            }
            catch (Exception ex)
            { }

            

            return ds;






        }
    }
}