﻿namespace NewPortalContratos.Web.Controllers.Common
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    public static class Common
    {
        public static string GetStringConnection(string Mandante) {
            string Conexion = string.Empty;

            //Quito los espacios a la cadena mandante y la transformo a mayúscula
            Mandante = Mandante.ToUpper().Trim().Replace(" ", string.Empty);

            //Obtengo la conexión
            try {
                Conexion = ConfigurationManager.ConnectionStrings[Mandante].ConnectionString.ToString().Trim();
            }
            catch (Exception ex) {
                Conexion = string.Empty;
            }

            return Conexion;
        }

        public static DataSet ConsultaSQL(string Conexion,string Procedimiento, SqlCommand dbCommand) {
            SqlConnection dbConnection = new SqlConnection(Conexion);
            dbCommand.CommandType = CommandType.StoredProcedure;
            dbCommand.CommandText = Procedimiento;
            dbCommand.Connection = dbConnection;
            dbCommand.CommandTimeout = 360;

            dbConnection.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            da.SelectCommand = dbCommand;
            da.Fill(ds);
            dbConnection.Close();
            dbCommand.Connection.Dispose();

            return ds;


        }

        public static int GetVersionMinima() {
            return int.Parse(ConfigurationManager.AppSettings["VersionMinima"].ToString().Trim());
        }

        public static int GetVersionActual()
        {
            return int.Parse(ConfigurationManager.AppSettings["VersionActual"].ToString().Trim());
        }

        public static string GetVersionActualNombre()
        {
            return ConfigurationManager.AppSettings["VersionActualNombre"].ToString().Trim();
        }

        public static string GetURL()
        {
            return ConfigurationManager.AppSettings["URL"].ToString().Trim();
        }

        public static string GetURLSoporte()
        {
            return ConfigurationManager.AppSettings["URLSoporte"].ToString().Trim();
        }

        public static string GetFirebaseKey()
        {
            return ConfigurationManager.AppSettings["FirebaseKey"].ToString().Trim();
        }

        public static string GetFirebaseUri()
        {
            return ConfigurationManager.AppSettings["FirebaseUri"].ToString().Trim();
        }
    }
}